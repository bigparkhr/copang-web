import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)

import ListFoodInfo from '~/components/list-food-info'
Vue.component('ListFoodInfo', ListFoodInfo)
